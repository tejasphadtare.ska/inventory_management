print("Car Inventory Management System")

from inventory.product import Customer, Product, ProductCategory, Quotation, Stock, Sale


print("User sets Product Category")
categories_list = [
    {'category_id' : 1, 'product_category' : "Sedan"},
    {'category_id' : 2, 'product_category' : "Coop"},
    {'category_id' : 3, 'product_category' : "Luxury"}
]

stocks_list = [{
            'stock_id' : 1,
            'product_id' : 1,
            'quantity' : 30,
            'location' : "Thacker Tower, Sector 17, Vashi, Mumbai, Maharashtra Zip code  400705",
            'barcode' : "acvdvdsvsdbs"
        },
        {
            'stock_id' : 2,
            'product_id' : 2,
            'quantity' : 300,
            'location' : "97, B G T Road, Bally, Kolkata, West Bengal Zip code -712232",
            'barcode' : "acvdvdsvshasddbs"
        }]

products_list = [{
        'product_id' : 1,
        'name' : "Verna",
        'category_id' : 1,
        'price' : 1500000,
        'description' : "Sedan with curse mode",
        'unit_of_measure' : "kgs",
        'product_type' : "Available",
        'in_stock':True,
        'color':"Black",
        'discount':3,
        'manufacturer':"Hyundai",
        'item_weight':200,
        'country_of_origin':"India"
        },
        {
        'product_id' : 2,
        'name' : "i20",
        'category_id' : 2,
        'price' : 1000000,
        'description' : "Coop with sports mode",
        'unit_of_measure' : "kgs",
        'product_type' : "Available",
        'in_stock':True,
        'color':"Blue",
        'discount':5,
        'manufacturer':"Hyundai",
        'item_weight':150,
        'country_of_origin':"China"
        },
        {
        'product_id' : 3,
        'name' : "Lamborgini",
        'category_id' : 3,
        'price' : 20000000,
        'description' : "Luxury sports car",
        'unit_of_measure' : "kgs",
        'product_type' : "Not Available",
        'in_stock':False,
        'color':"NA",
        'discount':0,
        'manufacturer':"Jaguar",
        'item_weight':250,
        'country_of_origin':"India"
        }]

customers_list = [{
    'customer_id' : 1,
    'customer_name' : "John",
    'email' : "john@domain.com",
    'address' : "O - 38, Part 2, Lajpat Nagar, Delhi Zip code  110024",
    'phone_number' : 9123457122,
    'alternate_number' : 1126836731,
    'shipping_address' : "Universal Indl Est, Goregaon (east), Mumbai, Maharashtra Zip code  400063",
    'quote_confirmation' : False
},
{
    'customer_id' : 2,
    'customer_name' : "Jane",
    'email' : "jane@domain.com",
    'address' : " J - 89, Part 4, Lajpat Nagar, Delhi Zip code  110024",
    'phone_number' : 9123444142,
    'alternate_number' : 1446836731,
    'shipping_address' : "Est, Ghatkopar, Mumbai, Maharashtra Zip code  400071",
    'quote_confirmation' : False
}
]

quotations_list = [{
    'quote_id' : 1,
    'product_id' : 1,
    'quantity' : 23,
    'customer_id' : 1,
    'start_date' : "08-08-2023",
    'state' : "Quotation",
    'discount' : 12,
}
]

sales_list = [{'sale_id' : 1,
    'product_id' : 1,
    'quantity' : 23,
    'customer_id' : 1,
    'new_date' : "11-09-2023",
    'invoice_address' : "PO 1, Hill Towers, Mumbai",
    'delivery_address' : "F-201, River Residency, Pune",
    'payment_status' : "Not Done",
    'taxes' : 2.5,
    'total' : 0
}]

store_to_db = True

if store_to_db == True:
    #Setting Categories
    categories = ProductCategory.create_category(categories_list)
    ProductCategory.insert_category_to_db(categories)

    # #Setting Stocks
    stocks = Stock.create_stock(stocks_list)
    Stock.insert_stock_to_db(stocks)

    # #Setting Products
    products = Product.create_product(products_list)
    Product.insert_product_to_db(products)

    # #Setting Customers
    customers=Customer.create_customer(customers_list)
    Customer.insert_customer_to_db(customers)

    """ 
    Upon customer request generate quotation
    """

    # #Setting Quotations
    quotations=Quotation.create_quotation(quotations_list)
    Quotation.insert_quotation_to_db(quotations)

    """ 
    Upon customer confirmation set quote_state to Sale Order
    """
    Customer.update_customer_to_db(customer_id=1,new_quote_confirmation=True)

    """ 
    Note: Rest of the flow is pending for DB approach
    """
 


else:
    print("Excution is not saving data to Database a store_to_db is set to False")
    print("=====================================================================")
    categories = ProductCategory.create_category(categories_list)
    stocks = Stock.create_stock(stocks_list)

    print("Product details and Stock information")
    products = Product.create_product(products_list)
    for prod in Product.instances:
        print("===============================================================")
        print("Fetching the Product category name based on product category id")
        print("===============================================================")
        print("Product Details :::",prod.get_product_category_name(categories, prod.category_id))
        print("Stock Info :::",prod.get_stock_info(stocks, prod.product_id))

    """
    Quotation creation after customer request for a quote 
    """
    customers=Customer.create_customer(customers_list)

    quotations=Quotation.create_quotation(quotations_list)

    """
    Update a customer with quote confirmation to True
    """

    Customer.update_customer(customers, 
                            customer_id = 1, 
                            new_name = 'John Brown',
                            new_quote_confirmation = True)


    """
    Based Upon customer input to proceed with Sale_Order,
    Calculate Total_price, set state to "Sale_Order"
    and update stock quantity
    """
    print("If customer gives quote confirmation update quote status to Sale_Order")
    for quote in Quotation.instances:
        for customer in Customer.instances:
            quote.total_price = quote.get_total_price(products)
            if customer.customer_id == quote.customer_id and customer.quote_confirmation is not False:
                quote.state = "Sale_Order"
                for stock in Stock.instances:
                    if quote.product_id == stock.product_id:
                        stock.set_stock_quantity(stocks, quote.quantity)
            else:
                "Quote not present for the customer or Not in !"


    for quote in Quotation.instances:
        print("=====================")
        print("Quotation State Check")
        print("=====================")
        print("Quotation Id ",quote.quote_id)
        print("Quotation Quantity ",quote.quantity)
        print("Quotation State ", quote.state)

    #Check if stock quantity updates in instance
    for stock in Stock.instances:
        print("=====================")
        print("Stock quantity Check as quote state changed to Sale_Order")
        print("=====================")
        print("Product Id ",stock.product_id)
        print("Stock Location ", stock.location)
        print("Stock Quantity ",stock.quantity)


    """
    Check Quote state and total_price
    """
    for quote in Quotation.instances:
        print("=====================")
        print("Quote ID = "+str(quote.quote_id))
        print("Quote Total Price after discount = "+str(quote.total_price))
        print("=====================")

    """
    Check Sale Receipt
    """
    Quotation.read_all_quotations(quotations)
    sales = Sale.create_sale(sales_list)
    print("Payment status")
    print("=====================")

    for quote in Quotation.instances:
        for sale in Sale.instances:
            if quote.customer_id == sale.customer_id:
                temp_total = quote.total_price * (sale.taxes/100)
                sale.total = int(quote.total_price) + int(temp_total)
                quote.state = "Invoiced"

    Sale.read_all_sale(sales)

    """ 
    Update Payment Status to Done after customer confirmation
    """
    print("Updated sale with payment status")
    print("=====================")
    Sale.update_sale(sales,1,23,1,"11-09-2023","PO 1, Hill Towers, Mumbai","F-201, River Residency, Pune","Done",2.5,sale.total)

    Sale.read_all_sale(sales)
    Quotation.read_all_quotations(quotations)
    print("=====================")




