from inventory.product import ProductCategory, Stock, Customer, Quotation, Sale, Product

# categories_list = [
#     {'category_id' : 10, 'product_category' : "Transport"},
#     {'category_id' : 11, 'product_category' : "Logistics"}
# ]
""" 
Inserting category into Inventory DB
"""
# categories = ProductCategory.create_category(categories_list)
# ProductCategory.insert_category_to_db(categories)
# print("Insert Done")

""" 
Reading from Inventory DB
"""
# print(ProductCategory.read_category_from_db())

""" 
Updating data to DB
"""
# print(ProductCategory.update_category_to_db("product_category","Hatchback","category_id=2"))

""" 
Deleting data from DB
"""
# ProductCategory.delete_category_from_db("id=8")


""" 
Inserting Stock into Inventory DB
"""

# stocks_list = [{
#             'stock_id' : 3,
#             'product_id' : 3,
#             'quantity' : 50,
#             'location' : "Khargar, Navi Mumbai",
#             'barcode' : "asdasfkjavkllasc"
#         },]
# stocks = Stock.create_stock(stocks_list)
# Stock.insert_stock_to_db(stocks)
# print("Stock Insert Done")

""" 
Reading from Inventory DB
"""
# print(Stock.read_stock_from_db())

""" 
Updating Stock data to DB
"""
# print(Stock.update_stock_to_db(stock_id=2, new_quantity=1600, new_location="29, Doha, West Bengal"))


# Stock.delete_stock_from_db("stock_id=3")

""" 
Inserting Customer into Inventory DB
"""

customers_list = [{
    'customer_id' : 3,
    'customer_name' : "Bob",
    'email' : "bob@domain.com",
    'address' : "O - 38, Part 2, Lajpat Nagar, Delhi Zip code  110024",
    'phone_number' : 9123457122,
    'alternate_number' : 1126836731,
    'shipping_address' : "Universal Indl Est, Goregaon (east), Mumbai, Maharashtra Zip code  400063",
    'quote_confirmation' : False
}]

# Create customers using the Customer class method
# customers = Customer.create_customer(customers_list)
# Customer.insert_customer_to_db(customers)
# print("Customer Insert Done")

""" 
Reading Customer from Inventory DB
"""
# print(Customer.read_customer_from_db())

# Customer.update_customer_to_db(customer_id=2,new_quote_confirmation=False)

# Customer.delete_customer_from_db(condition="customer_id=3")


quotations_list = [
#     {
#     'quote_id' : 1,
#     'product_id' : 1,
#     'quantity' : 23,
#     'customer_id' : 1,
#     'start_date' : "08-08-2023",
#     'state' : "Quotation",
# },
{   
    'quote_id' : 2,
    'product_id' : 1,
    'quantity' : 5,
    'customer_id' : 2,
    'start_date' : "08-08-2023",
    'state' : "Quotation",
}
]

# quotations=Quotation.create_quotation(quotations_list)
# Quotation.insert_quotation_to_db(quotations)

quote_info = Quotation.read_quotation_from_db()
for quote in quote_info:
    print(quote[-2])
# Quotation.update_quotation_to_db(quote_id=1, new_state="Quotation")
""" 
Inserting Customer into Inventory DB
"""

sale_list = [{'sale_id' : 1,
    'product_id' : 1,
    'quantity' : 23,
    'customer_id' : 1,
    'new_date' : "11-09-2023",
    'invoice_address' : "PO 1, Hill Towers, Mumbai",
    'delivery_address' : "F-201, River Residency, Pune",
    'payment_status' : "Not Done",
    'taxes' : 2.5,
    'total' : 0
}]

sale = Sale.create_sale(sale_list)
# print(Sale.insert_sale_to_db(sale))

# print(Sale.read_sale_from_db())

# Sale.update_sale_to_db(sale_id=1,new_payment_status="Done")

# Sale.delete_sale_from_db(condition='id=2')


products_list = [{
        'product_id' : 4,
        'name' : "Verna",
        'category_id' : 1,
        'price' : 1500000,
        'description' : "Sedan with curse mode",
        'unit_of_measure' : "kgs",
        'product_type' : "Available",
        'in_stock':True,
        'color':"Black",
        'discount':3,
        'manufacturer':"Hyundai",
        'item_weight':200,
        'country_of_origin':"India"
        },
        {
        'product_id' : 5,
        'name' : "i20",
        'category_id' : 2,
        'price' : 1000000,
        'description' : "Coop with sports mode",
        'unit_of_measure' : "kgs",
        'product_type' : "Available",
        'in_stock':True,
        'color':"Blue",
        'discount':5,
        'manufacturer':"Hyundai",
        'item_weight':150,
        'country_of_origin':"China"
        },
        {
        'product_id' : 6,
        'name' : "Lamborgini",
        'category_id' : 3,
        'price' : 20000000,
        'description' : "Luxury sports car",
        'unit_of_measure' : "kgs",
        'product_type' : "Not Available",
        'in_stock':False,
        'color':"NA",
        'discount':0,
        'manufacturer':"Jaguar",
        'item_weight':250,
        'country_of_origin':"India"
        }]

products = Product.create_product(products_list)
# print(Product.insert_product_to_db(products))

# print(Product.read_product_from_db())

# Product.update_sale_to_db(product_id=4,new_price=4500000)

# Product.delete_product_from_db(condition='id>3')