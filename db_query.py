import psycopg2

# Establishing a connection to the PostgreSQL database
conn = psycopg2.connect(
    host="127.0.0.1",
    database="inventory",
    user="postgres",
    password="password"
)

#cursor
cur = conn.cursor()

""" To Create a table in Postgresql"""

# Product Category table
try:
    cur.execute("CREATE TABLE product_category (id serial PRIMARY KEY, category_id integer ,product_category varchar);")
except:
    print("I can't create our table!")

# Product table
try:
    cur.execute("CREATE TABLE product (id serial PRIMARY KEY, product_id integer, name varchar, category_id integer, price integer, description varchar, unit_of_measure varchar, product_type varchar, in_stock boolean, color varchar, discount integer, item_weight integer, manufacturer varchar, country_of_origin varchar, FOREIGN KEY (category_id) REFERENCES product_category(id));")
except(Exception, psycopg2.Error) as error:
    print("I can't create our table!",error)


# # Stock  table
try:
    cur.execute("CREATE TABLE stock (id serial PRIMARY KEY,stock_id integer, product_id integer, quantity integer, location varchar, barcode varchar, FOREIGN KEY (product_id) REFERENCES product(id));")
except:
    print("I can't create our table!")

# customer  table
try:
    cur.execute("CREATE TABLE customer (id serial PRIMARY KEY, customer_id integer, customer_name varchar, email varchar, address varchar, phone_number integer, alternate_number integer, shipping_address varchar, quote_confirmation boolean);")
except:
    print("I can't create our table!")

# quotation  table
try:
    cur.execute("CREATE TABLE quotation (id serial PRIMARY KEY, quote_id integer, product_id integer, quantity integer, customer_id integer, start_date varchar, end_date varchar, state varchar, total_price integer,FOREIGN KEY (product_id) REFERENCES product(id),FOREIGN KEY (customer_id) REFERENCES customer(id));")
except:
    print("I can't create our table!")


# sale  table
try:
    cur.execute("CREATE TABLE sale (id serial PRIMARY KEY, sale_id integer, product_id integer, quantity integer, customer_id integer, date varchar, invoice_address varchar, delivery_address varchar, payment_status varchar,taxes integer, total integer,FOREIGN KEY (product_id) REFERENCES product(id),FOREIGN KEY (customer_id) REFERENCES customer(id));")
except:
    print("I can't create our table!")


conn.commit()

cur.close()
conn.close()