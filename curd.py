from inventory.product import Customer, Product, Sale, ProductCategory


""" 
Customer related CURD Operations
"""
categories_list = [
    {'category_id' : 1, 'product_category' : "Electronics"},
    {'category_id' : 2, 'product_category' : "Clothing"},
    {'category_id' : 3, 'product_category' : "Home Decor"}
]
categories = ProductCategory.create_category(categories_list)

print(ProductCategory.read_all_categories(categories))

""" 
Customer related CURD Operations
"""

customers_list = [{
    'customer_id' : 1,
    'customer_name' : "John",
    'email' : "john@domain.com",
    'address' : "O - 38, Part 2, Lajpat Nagar, Delhi Zip code  110024",
    'phone_number' : 9123457122,
    'alternate_number' : 1126836731,
    'shipping_address' : "Universal Indl Est, Goregaon (east), Mumbai, Maharashtra Zip code  400063",
    'quote_confirmation' : False
},
{
    'customer_id' : 2,
    'customer_name' : "Jane",
    'email' : "jane@domain.com",
    'address' : " J - 89, Part 4, Lajpat Nagar, Delhi Zip code  110024",
    'phone_number' : 9123444142,
    'alternate_number' : 1446836731,
    'shipping_address' : "Est, Ghatkopar, Mumbai, Maharashtra Zip code  400071",
    'quote_confirmation' : False
}]

# Create customers using the Customer class method
customers = Customer.create_customer(customers_list)


# Read all customers
Customer.read_all_customers(customers)

# Update a customer with quote confirmation to True
Customer.update_customer(customers, 1, 
                         'John Brown', 
                         "john_brown@mail.com",
                         "Lajpat Nagar, Delhi Zip code  110024", 
                         9123444142,
                         None, 
                         "Ghatkopar, Mumbai, Maharashtra Zip code  400071", 
                         True)

# Delete a customer
Customer.delete(customers, 2)

# Read all customers
Customer.read_all_customers(customers)

""" 
Product related CURD Operations
"""
products_list = [{
        'product_id' : 1,
        'name' : "SmartPhone",
        'category_id' : 1,
        'price' : 60000,
        'description' : "Communication device",
        'unit_of_measure' : "grams",
        'product_type' : "Available",
        'in_stock':True,
        'color':"Black",
        'discount':20,
        'manufacturer':"Apple",
        'item_weight':200,
        'country_of_origin':"India"
        },
        {
        'product_id' : 2,
        'name' : "Jacket",
        'category_id' : 2,
        'price' : 3000,
        'description' : "Colthing ware",
        'unit_of_measure' : "grams",
        'product_type' : "Available",
        'in_stock':True,
        'color':"Blue",
        'discount':10,
        'manufacturer':"Levis",
        'item_weight':100,
        'country_of_origin':"China"
        },
        {
        'product_id' : 3,
        'name' : "Chair",
        'category_id' : 3,
        'price' : 6000,
        'description' : "Home and Decoration",
        'unit_of_measure' : "kgs",
        'product_type' : "Not Available",
        'in_stock':False,
        'color':"Gray",
        'discount':5,
        'manufacturer':"LVR Enterprises",
        'item_weight':3,
        'country_of_origin':"India"
        }]

# Create products using the Product class method
products = Product.create_product(products_list)

# Read all Products
Product.read_all_products(products)

# Update a Products
Product.update_product(products, 2, 'Jeans', 2, 4000,"Clothing and Comfort","grams", "Available", True, "All", 0,"H&M",150,"USA")

# Read all Products
Product.read_all_products(products)

print("Deleting Product 2")
# Delete a Products
Product.delete(products, 2)

# Read all Products
Product.read_all_products(products)



""" 
Sale related CURD Operations
"""
sales_list = [{
    'sale_id' : 1,
    'product_id' : 1,
    'quantity' : 23,
    'customer_id' : 1,
    'new_date' : "10-09-2023",
    'invoice_address' : "PO 1, Hill Towers, Mumbai",
    'delivery_address' : "F-201, River Residency, Pune",
    'payment_status' : "Not Done",
    'taxes' : 2.5,
    'total' : 0
}]

sales = Sale.create_sale(sales_list)

Sale.read_all_sale(sales)

Sale.update_sale(sales,1,23,1,"11-09-2023","PO 1, Hill Towers, Mumbai","F-201, River Residency, Pune","Done",2.5,0)

Sale.read_all_sale(sales)


