from marshmallow import Schema, fields, post_load

from inventory.product import (
    Product, 
    ProductCategory, 
    Stock, 
    WarehouseLocation, 
    Customer,
    Quotation,
    Sale
    )

__all__ = ["ProductSchema"]

""" Schema Classes"""
class ProductSchema(Schema):
    product_id = fields.Integer()
    name = fields.String()
    category_id = fields.Integer()
    price = fields.Integer()
    description = fields.String()
    unit_of_measure = fields.String()
    product_type = fields.String()
    in_stock = fields.Boolean()
    color = fields.String()
    discount = fields.Integer()
    manufacturer = fields.String()
    item_weight = fields.Integer()
    country_of_origin = fields.String()

    @post_load
    def create_product(self, data, **kwargs):
        return Product(**data)
    
class ProductCategorySchema(Schema):
    category_id = fields.Integer()
    product_category = fields.String()

    @post_load
    def create_product_category(self, data, **kwargs):
        return ProductCategory(**data)


class StockSchema(Schema):
    stock_id = fields.Integer()
    product_id = fields.Integer()
    quantity = fields.Integer()
    location = fields.String()
    barcode = fields.String()

    @post_load
    def create_stock(self, data, **kwargs):
        return Stock(**data)

class CustomerSchema(Schema):
    customer_id = fields.Integer()
    customer_name = fields.String()
    email = fields.String()
    address = fields.String()
    phone_number = fields.Integer()
    alternate_number = fields.Integer()
    shipping_address = fields.String()
    quote_confirmation = fields.Boolean()

    @post_load
    def create_customer(self, data, **kwargs):
        return Customer(**data)
    
class QuotationSchema(Schema):
    quote_id = fields.Integer()
    product_id = fields.Integer()
    quantity = fields.Integer()
    customer_id = fields.Integer()
    start_date = fields.String()
    end_date = fields.String()
    state = fields.String()
    total_price = fields.Integer()

    @post_load
    def create_quotation(self, data, **kwargs):
        return Quotation(**data)
    

class SaleSchema(Schema):
    sale_id = fields.Integer()
    product_id = fields.Integer()
    quantity = fields.Integer()
    customer_id = fields.Integer()
    date = fields.String()
    invoice_address = fields.String()
    delivery_address = fields.String()
    payment_status = fields.String()
    taxes = fields.Integer()
    total = fields.Integer()

    @post_load
    def create_sale(self, data, **kwargs):
        return Sale(**data)