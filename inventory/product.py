""" 
Inventory Management System
"""
import psycopg2

__all__ = ["ProductCategory","Product"]

class ProductCategory:
    """ 
    Python class that is representation of Product category
    to which a Product belongs to in inventory
    """
    conn = psycopg2.connect(
                host="127.0.0.1",
                database="inventory",
                user="postgres",
                password="password"
            )
    instances = []

    def __init__(self, category_id : int = None, product_category : str = None):
        self._category_id = category_id
        self._product_category = product_category

    @property
    def category_id(self):
        return self._category_id

    @category_id.setter
    def category_id(self, category_id):
        self._category_id = category_id

    @property
    def product_category(self):
        return self._product_category

    @product_category.setter
    def product_category(self, product_category):
        self._product_category = product_category

    @classmethod
    def create_category(cls, category_data):
        for data in category_data:
            category = cls(
                data.get('category_id'),
                data.get('product_category')
                )
            cls.instances.append(category)
        return cls.instances
    
    @classmethod
    def read_all_categories(cls, categories):
        for category in categories:
            print(f"Category ID: {category.category_id}")
            print(f"Category Name: {category.product_category}")
            print()

    @classmethod
    def update(cls, categories, category_id, new_product_category):
        for category in categories:
            if category.category_id == category_id:
                category.product_category = new_product_category
                break

    @classmethod
    def delete(cls, categories, category_id):
        for category in categories:
            if category.category_id == category_id:
                categories.remove(category)
                break
    
    @classmethod
    def insert_category_to_db(cls, category_data):
        try:
            for data in category_data:
                category_id = data.category_id
                product_category = data.product_category
                conn = cls.conn
                cur = conn.cursor()
                cur.execute("INSERT INTO product_category (category_id, product_category) VALUES (%s, %s)", (category_id, product_category))
                conn.commit()
            print("Category inserted to DB.")
            cur.close()
            conn.close()
        except (Exception, psycopg2.Error) as error:
            print("Error while inserting data:", error)
    
    @classmethod
    def read_category_from_db(cls):
        try:
            data = []
            select_query = f"SELECT * FROM product_category"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            for row in rows:
                data.append(row)
            return data
        except (Exception, psycopg2.Error) as error:
            print("Error while selecting data:", error)

    @classmethod
    def update_category_to_db(cls,column_name,new_value,condition):
        try:
            update_query = f"UPDATE product_category SET {column_name} = %s WHERE {condition}"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(update_query,(new_value,))
            conn.commit()
            cur.close()
            conn.close()
            print(f"{column_name} is Updated")
        except (Exception, psycopg2.Error) as error:
            print("Error while updating data:", error)

    @classmethod
    def delete_category_from_db(cls,condition):
        try:
            delete_query = f"DELETE FROM product_category WHERE {condition}"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(delete_query)
            conn.commit()
            cur.close()
            conn.close()
            print(f"Record is Deleted")
        except (Exception, psycopg2.Error) as error:
            print("Error while deleting data:", error)

class Stock:
    """ 
    Python class that is representation of Stock in inventory
    """
    conn = psycopg2.connect(
                host="127.0.0.1",
                database="inventory",
                user="postgres",
                password="password"
            )
    
    instances = []

    def __init__(
            self, 
            stock_id : int = None,
            product_id : int = None, 
            quantity : int = None, 
            location : str = None,
            barcode : str = None
        ):
        self._stock_id = stock_id
        self._product_id = product_id
        self._quantity = quantity
        self._location = location
        self._barcode = barcode

    @property
    def stock_id(self):
        return self._stock_id

    @stock_id.setter
    def stock_id(self, stock_id):
        self._stock_id = stock_id

    @property
    def product_id(self):
        return self._product_id

    @product_id.setter
    def product_id(self, product_id):
        self._product_id = product_id

    @property
    def quantity(self):
        return self._quantity

    @quantity.setter
    def quantity(self, quantity):
        self._quantity = quantity

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, location):
        self._location = location

    @property
    def barcode(self):
        return self._barcode

    @barcode.setter
    def barcode(self, barcode):
        self._barcode = barcode

    @classmethod
    def create_stock(cls, stock_data):
        for data in stock_data:
            stock = cls(
                data.get('stock_id'),
                data.get('product_id'),
                data.get('quantity'),
                data.get('location'),
                data.get('barcode')
                )
            cls.instances.append(stock)
        return cls.instances
    
    @classmethod
    def read_all_stock(cls, stocks):
        for stock in stocks:
            print(f"Stock ID: {stock.stock_id}")
            print(f"Product ID: {stock.product_id}")
            print(f"Quantity: {stock.quantity}")
            print(f"Location: {stock.location}")
            print(f"Barcode: {stock.barcode}")
            print()

    @classmethod
    def update(cls, stocks, product_id, new_quantity=None, new_location=None, new_barcode=None):
        for stock in stocks:
            if stock.product_id == product_id:
                if new_quantity is not None:
                    stock.quantity = new_quantity
                if new_location is not None:
                    stock.location = new_location
                if new_barcode is not None:
                    stock.barcode = new_barcode
                break

    @classmethod
    def delete(cls, stocks, product_id):
        for stock in stocks:
            if stock.product_id == product_id:
                stocks.remove(stock)
                break
            else:
                return "Product not found"

    def set_stock_quantity(self, stocks, quote_quantity):
        for stock in stocks:
            if stock.product_id  == self.product_id:
                self._quantity = self._quantity - quote_quantity
                return "Stock quantity updated"
        return "Product not found"
    
    @classmethod
    def insert_stock_to_db(cls, stock_data):
        try:
            for data in stock_data:
                stock_id = data.stock_id
                product_id = data.product_id
                quantity = data.quantity
                location = data.location
                barcode = data.barcode
                conn = cls.conn
                cur = conn.cursor()
                cur.execute("INSERT INTO stock (stock_id, product_id, quantity, location, barcode) VALUES (%s, %s, %s, %s, %s)", (stock_id, product_id, quantity, location, barcode))
                conn.commit()
            print("Stock inserted to DB.")
            cur.close()
            conn.close()
        except (Exception, psycopg2.Error) as error:
            print("Error while inserting stock data:", error)
    
    @classmethod
    def read_stock_from_db(cls):
        try:
            data = []
            select_query = f"SELECT * FROM stock"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            for row in rows:
                data.append(row)
            return data
        except (Exception, psycopg2.Error) as error:
            print("Error while selecting data from stock:", error)

    @classmethod
    def update_stock_to_db(cls, stock_id, new_product_id=None, new_quantity=None, new_location=None, new_barcode=None):
        try:
            update_query = """
            UPDATE stock 
            SET product_id = COALESCE(%s, product_id), 
                quantity = COALESCE(%s, quantity), 
                location = COALESCE(%s, location), 
                barcode = COALESCE(%s, barcode)
            WHERE stock_id = %s
            """
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(update_query, (new_product_id, new_quantity, new_location, new_barcode, stock_id))
            conn.commit()
            # print(cls.read_stock_from_db())
            cur.close()
            conn.close()
            print("Stock Updated")
        except (Exception, psycopg2.Error) as error:
            print("Error while updating data:", error)

    @classmethod
    def delete_stock_from_db(cls,condition):
        try:
            delete_query = f"DELETE FROM stock WHERE {condition}"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(delete_query)
            conn.commit()
            cur.close()
            conn.close()
            print(f"Stock record is Deleted")
        except (Exception, psycopg2.Error) as error:
            print("Error while stock deleting data:", error)

class Customer:
    """ 
    Python class that is representation of Customer/Client
    related information
    """
    conn = psycopg2.connect(
                host="127.0.0.1",
                database="inventory",
                user="postgres",
                password="password"
            )

    instances = []

    def __init__(
            self, 
            customer_id : int = None,
            customer_name : str = None, 
            email : str = None,
            address : str = None, 
            phone_number : int = None,
            alternate_number : int = None,
            shipping_address : str = None,
            quote_confirmation : bool = None
            ):
        self._customer_id = customer_id
        self._customer_name = customer_name
        self._email = email
        self._address = address
        self._phone_number = phone_number
        self._alternate_number = alternate_number
        self._shipping_address = shipping_address
        self._quote_confirmation = quote_confirmation

    @property
    def customer_id(self):
        return self._customer_id
    
    @customer_id.setter
    def customer_id(self,customer_id):
        self._customer_id = customer_id

    @property
    def quote_confirmation(self):
        return self._quote_confirmation
    
    @quote_confirmation.setter
    def quote_confirmation(self,quote_confirmation):
        self._quote_confirmation = quote_confirmation

    @property
    def customer_name(self):
        return self._customer_name

    @customer_name.setter
    def customer_name(self, customer_name):
        self._customer_name = customer_name

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        self._email = email

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, address):
        self._address = address

    @property
    def phone_number(self):
        return self._phone_number

    @phone_number.setter
    def phone_number(self, phone_number):
        self._phone_number = phone_number

    @property
    def alternate_number(self):
        return self._alternate_number

    @alternate_number.setter
    def alternate_number(self, alternate_number):
        self._alternate_number = alternate_number

    @property
    def shipping_address(self):
        return self._shipping_address

    @shipping_address.setter
    def shipping_address(self, shipping_address):
        self._shipping_address = shipping_address

    @classmethod
    def create_customer(cls, customer_data):
        for data in customer_data:
            customer = cls(data.get('customer_id'),
                        data.get('customer_name'),
                        data.get('email'),
                        data.get('address'),
                        data.get('phone_number'),
                        data.get('alternate_number'),
                        data.get('shipping_address'),
                        data.get('quote_confirmation')
                        )
            cls.instances.append(customer)
        return cls.instances

    @classmethod
    def read_all_customers(cls,customers):
        for customer in customers:
            print(f"Customer ID: {customer.customer_id}")
            print(f"Name: {customer.customer_name}")
            print(f"Email: {customer.email}")
            print(f"address: {customer.address}")
            print(f"phone_number: {customer.phone_number}")
            print(f"alternate_number: {customer.alternate_number}")
            print(f"shipping_address: {customer.shipping_address}")
            print(f"quote_confirmation: {customer.quote_confirmation}")
            print()
    
    @classmethod
    def update_customer(
        cls, 
        customers, 
        customer_id,
        new_name=None, 
        new_email=None, 
        new_address=None, 
        new_phone_number=None, 
        new_alternate_number=None, 
        new_shipping_address=None,
        new_quote_confirmation=None
        ):
        for customer in customers:
            if customer.customer_id == customer_id:
                if new_name is not None:
                    customer.name = new_name
                if new_email is not None:
                    customer.email = new_email
                if new_address is not None:
                    customer.address = new_address
                if new_phone_number is not None:
                    customer.phone_number= new_phone_number
                if new_alternate_number is not None:
                    customer.alternate_number = new_alternate_number
                if new_shipping_address is not None:
                    customer.shipping_address = new_shipping_address
                if new_quote_confirmation is not None:
                    customer.quote_confirmation = new_quote_confirmation
                break

    
    @classmethod
    def delete(cls, customers, customer_id):
        for customer in customers:
            if customer.customer_id == customer_id:
                customers.remove(customer)
                print("Customer Deleted!!!")
                break
            else:
                return "Customer Not Found."
            
    @classmethod
    def insert_customer_to_db(cls, customer_data):
        try:
            for data in customer_data:
                customer_id = data.customer_id
                customer_name = data.customer_name
                email = data.email
                address = data.address
                phone_number = data.phone_number
                alternate_number = data.alternate_number
                shipping_address = data.shipping_address
                quote_confirmation = data.quote_confirmation
                conn = cls.conn
                cur = conn.cursor()
                cur.execute("INSERT INTO customer (customer_id, customer_name, email, address, phone_number, alternate_number,shipping_address,quote_confirmation) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (customer_id, customer_name, email, address, phone_number, alternate_number,shipping_address,quote_confirmation))
                conn.commit()
            print("Customer inserted to DB.")
            cur.close()
            conn.close()
        except (Exception, psycopg2.Error) as error:
            print("Error while inserting customer data:", error)

    @classmethod
    def read_customer_from_db(cls):
        try:
            data = []
            select_query = f"SELECT * FROM customer"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            for row in rows:
                data.append(row)
            return data
        except (Exception, psycopg2.Error) as error:
            print("Error while selecting data from customer:", error)

    @classmethod
    def update_customer_to_db(cls, 
                            customer_id,
                            new_customer_name=None, 
                            new_email=None, 
                            new_address=None, 
                            new_phone_number=None,
                            new_alternate_number=None,
                            new_shipping_address=None,
                            new_quote_confirmation=None
                            ):
        try:
            update_query = """
            UPDATE customer 
            SET customer_name = COALESCE(%s, customer_name), 
                email = COALESCE(%s, email), 
                address = COALESCE(%s, address), 
                phone_number = COALESCE(%s, phone_number),
                alternate_number = COALESCE(%s, alternate_number), 
                shipping_address = COALESCE(%s, shipping_address), 
                quote_confirmation = COALESCE(%s, quote_confirmation)
            WHERE customer_id = %s
            """
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(update_query, (new_customer_name, new_email, new_address, new_phone_number, new_alternate_number,new_shipping_address,new_quote_confirmation, customer_id))
            conn.commit()
            cur.close()
            conn.close()
            print("Customer Updated")
        except (Exception, psycopg2.Error) as error:
            print("Error while updating customer data:", error)

    @classmethod
    def delete_customer_from_db(cls,condition):
        try:
            delete_query = f"DELETE FROM customer WHERE {condition}"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(delete_query)
            conn.commit()
            cur.close()
            conn.close()
            print(f"Customer record is Deleted")
        except (Exception, psycopg2.Error) as error:
            print("Error while deleting customer data:", error)

class Quotation:
    """ 
    Python class that is representation of Quotations requested 
    by a Client for a Product/Products in a inventory
    """
    conn = psycopg2.connect(
                host="127.0.0.1",
                database="inventory",
                user="postgres",
                password="password"
            )
    instances = []

    def __init__(
            self,
            quote_id : int = None, 
            product_id : int = None, 
            quantity : int = None,
            customer_id : int = None,
            start_date : str = None,
            end_date : str = None,
            state : str = "Quotation",
            discount : int = None,
            total_price : int = None
            ):
        self._quote_id = quote_id
        self._product_id = product_id
        self._quantity = quantity
        self._customer_id = customer_id
        self._start_date = start_date
        self._end_date = end_date
        self._state = state
        self._discount = discount
        self._total_price = total_price
    
    @property
    def quote_id(self):
        return self._quote_id

    @quote_id.setter
    def quote_id(self, quote_id):
        self._quote_id = quote_id
    
    @property
    def product_id(self):
        return self._product_id

    @product_id.setter
    def product_id(self, product_id):
        self._product_id = product_id

    @property
    def quantity(self):
        return self._quantity

    @quantity.setter
    def quantity(self, quantity):
        self._quantity = quantity

    @property
    def customer_id(self):
        return self._customer_id

    @customer_id.setter
    def customer_id(self, customer_id):
        self._customer_id = customer_id

    @property
    def start_date(self):
        return self._start_date

    @start_date.setter
    def start_date(self, start_date):
        self._start_date = start_date

    @property
    def end_date(self):
        return self._end_date

    @end_date.setter
    def end_date(self, end_date):
        self._end_date = end_date

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = state

    @property
    def total_price(self):
        return self._total_price

    @total_price.setter
    def total_price(self, total_price):
        self._total_price = total_price

    def get_total_price(self,products):
        for product in products:
            if product.product_id == self.product_id:
                temp_price = int(self.quantity) * int(product.price)
                discount_amount =  temp_price * (product.discount/100)
                self._total_price = temp_price - discount_amount
                return self._total_price
        return "Product not found"
    

    @classmethod
    def create_quotation(cls, quotation_data):
        quotation_list = []
        for data in quotation_data:
            quotation = cls(
                        data.get('quote_id'),
                        data.get('product_id'),
                        data.get('quantity'),
                        data.get('customer_id'),
                        data.get('start_date'),
                        data.get('end_date'),
                        data.get('state'),
                        data.get('total_price')
                        )
            cls.instances.append(quotation)
        return cls.instances
    
    @classmethod
    def read_all_quotations(cls, quotations):
        for quote in quotations:
            print(f"Quotation ID: {quote.quote_id}")
            print(f"Product ID: {quote.product_id}")
            print(f"Quantity: {quote.quantity}")
            print(f"Customer ID: {quote.customer_id}")
            print(f"Start Date: {quote.start_date}")
            print(f"End Date: {quote.end_date}")
            print(f"Quotation State: {quote.state}")
            print(f"Total Price: {quote.total_price}")
            print()

    @classmethod
    def update_quotation(
                cls, 
                quotations, 
                customer_id,
                new_quantity=None,
                new_product_id=None,
                new_start_date=None, 
                new_end_date=None, 
                new_state=None,
                new_total_price=None
               ):
        for quote in quotations:
            if quote.customer_id == customer_id:
                if new_quantity is not None:
                    quote.quantity = new_quantity
                if new_product_id is not None:
                    quote.product_id = new_product_id
                if new_start_date is not None:
                    quote.start_date = new_start_date
                if new_end_date is not None:
                    quote.end_date = new_end_date
                if new_state is not None:
                    quote.state = new_state
                if new_total_price is not None:
                    quote.total_price = new_total_price
                break

    @classmethod
    def delete_quotation(cls, quotations, customer_id):
        for quote in quotations:
            if quote.customer_id == customer_id:
                quotations.remove(quote)
                break
            else:
                return "Quotation Not Found."
            
    @classmethod
    def insert_quotation_to_db(cls, quotation_data):
        try:
            for data in quotation_data:
                quote_id = data.quote_id
                product_id = data.product_id
                quantity = data.quantity
                customer_id = data.customer_id
                start_date = data.start_date
                end_date = data.end_date
                state = data.state
                total_price = data.total_price
                conn = cls.conn
                cur = conn.cursor()
                cur.execute("INSERT INTO quotation (quote_id, product_id, quantity, customer_id, start_date, end_date, state,total_price) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (quote_id, product_id, quantity, customer_id, start_date, end_date, state,total_price))
                conn.commit()
            print("Quotation records inserted in quotation")
            cur.close()
            conn.close()
        except (Exception, psycopg2.Error) as error:
            print("Error while inserting quotation data:", error)

    @classmethod
    def read_quotation_from_db(cls):
        try:
            data = []
            select_query = f"SELECT * FROM quotation"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            for row in rows:
                data.append(row)
            return data
        except (Exception, psycopg2.Error) as error:
            print("Error while selecting data from quotation:", error)

    @classmethod
    def update_quotation_to_db(cls, 
                            quote_id,
                            new_product_id=None, 
                            new_quantity=None, 
                            new_customer_id=None, 
                            new_start_date=None,
                            new_end_date=None,
                            new_state=None,
                            new_total_price=None
                            ):
        try:
            update_query = """
            UPDATE quotation 
            SET product_id = COALESCE(%s, product_id), 
                quantity = COALESCE(%s, quantity), 
                customer_id = COALESCE(%s, customer_id), 
                start_date = COALESCE(%s, start_date),
                end_date = COALESCE(%s, end_date), 
                state = COALESCE(%s, state), 
                total_price = COALESCE(%s, total_price)
            WHERE customer_id = %s
            """
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(update_query, (new_product_id,new_quantity,new_customer_id,new_start_date,new_end_date,new_state,new_total_price,quote_id))
            conn.commit()
            cur.close()
            conn.close()
            print("Quotaion Updated")
        except (Exception, psycopg2.Error) as error:
            print("Error while updating quotation data:", error)

    @classmethod
    def delete_customer_from_db(cls,condition):
        try:
            delete_query = f"DELETE FROM quotation WHERE {condition}"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(delete_query)
            conn.commit()
            cur.close()
            conn.close()
            print(f"Quotation record is Deleted")
        except (Exception, psycopg2.Error) as error:
            print("Error while deleting quotation data:", error)

class Sale:
    """ 
    Python class that is representation of Sale done with a
    Client for a Product/Products in a inventory
    """
    conn = psycopg2.connect(
                host="127.0.0.1",
                database="inventory",
                user="postgres",
                password="password"
            )

    instances = []

    def __init__(
            self, 
            sale_id : int = None,
            product_id : int = None,
            quantity : int = None, 
            customer_id : int = None, 
            date : str = None, 
            invoice_address : str = None, 
            delivery_address : str = None, 
            payment_status : str = None, 
            taxes : int = None, 
            total : int = None):
        self._sale_id = sale_id
        self._product_id = product_id
        self._quantity = quantity
        self._customer_id = customer_id
        self._date = date
        self._invoice_address = invoice_address
        self._delivery_address = delivery_address
        self._payment_status = payment_status
        self._taxes = taxes
        self._total = total

    @property
    def sale_id(self):
        return self._sale_id

    @sale_id.setter
    def sale_id(self, value):
        self._sale_id = value

    @property
    def product_id(self):
        return self._product_id

    @product_id.setter
    def product_id(self, value):
        self._product_id = value

    @property
    def quantity(self):
        return self._quantity

    @quantity.setter
    def quantity(self, value):
        self._quantity = value

    @property
    def customer_id(self):
        return self._customer_id

    @customer_id.setter
    def customer_id(self, value):
        self._customer_id = value

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, value):
        self._date = value

    @property
    def invoice_address(self):
        return self._invoice_address

    @invoice_address.setter
    def invoice_address(self, value):
        self._invoice_address = value

    @property
    def delivery_address(self):
        return self._delivery_address

    @delivery_address.setter
    def delivery_address(self, value):
        self._delivery_address = value

    @property
    def payment_status(self):
        return self._payment_status

    @payment_status.setter
    def payment_status(self, value):
        self._payment_status = value

    @property
    def taxes(self):
        return self._taxes

    @taxes.setter
    def taxes(self, value):
        self._taxes = value

    @property
    def total(self):
        return self._total

    @total.setter
    def total(self, value):
        self._total = value


    @classmethod
    def create_sale(cls, sale_data):
        for data in sale_data:
            sale = cls( 
                        data.get('sale_id'),
                        data.get('product_id'),
                        data.get('quantity'),
                        data.get('customer_id'),
                        data.get('date'),
                        data.get('invoice_address'),
                        data.get('delivery_address'),
                        data.get('payment_status'),
                        data.get('taxes'),
                        data.get('total')
                        )
            cls.instances.append(sale)
        return cls.instances
    
    @classmethod
    def read_all_sale(cls, sales):
        for sale in sales:
            print(f"Sale ID: {sale.sale_id}")
            print(f"Product ID: {sale.product_id}")
            print(f"Quantity: {sale.quantity}")
            print(f"Customer ID: {sale.customer_id}")
            print(f"Date: {sale.date}")
            print(f"Invoice Address: {sale.invoice_address}")
            print(f"Delivery Address: {sale.delivery_address}")
            print(f"Payment Status: {sale.payment_status}")
            print(f"Taxes: {sale.taxes}")
            print(f"Total after taxes: {sale.total}")
            print()

    @classmethod
    def update_sale(cls, 
               sales, 
               customer_id,
               new_quantity=None,
               new_product_id=None, 
               new_date=None, 
               new_invoice_address=None,
               new_delivery_address=None,
               new_payment_status=None,
               new_taxes=None,
               new_total=None
               ):
        for sale in sales:
            if sale.customer_id == customer_id:
                if new_quantity is not None:
                    sale.quantity = new_quantity
                if new_product_id is not None:
                    sale.product_id = new_product_id
                if new_date is not None:
                    sale.date = new_date
                if new_invoice_address is not None:
                    sale.invoice_address = new_invoice_address
                if new_delivery_address is not None:
                    sale.delivery_address = new_delivery_address
                if new_payment_status is not None:
                    sale.payment_status = new_payment_status
                if new_taxes is not None:
                    sale.taxes = new_taxes
                if new_total is not None:
                    sale.total = new_total
                break

    @classmethod
    def delete_sale(cls, sales, customer_id):
        for sale in sales:
            if sale.customer_id == customer_id:
                sales.remove(sale)
                break

    @classmethod
    def insert_sale_to_db(cls, sale_data):
        try:
            for data in sale_data:
                sale_id = data.sale_id
                product_id = data.product_id
                quantity = data.quantity
                customer_id = data.customer_id
                date = data.date
                invoice_address = data.invoice_address
                delivery_address = data.delivery_address
                payment_status = data.payment_status
                taxes = data.taxes
                total = data.total
                conn = cls.conn
                cur = conn.cursor()
                cur.execute("INSERT INTO sale (sale_id, product_id, quantity, customer_id, date, invoice_address, delivery_address,payment_status, taxes, total) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (sale_id, product_id, quantity, customer_id, date, invoice_address, delivery_address,payment_status, taxes, total))
                conn.commit()
            print("Inserted records into sale  ")
            cur.close()
            conn.close()
        except (Exception, psycopg2.Error) as error:
            print("Error while inserting sale data:", error)

    @classmethod
    def read_sale_from_db(cls):
        try:
            data = []
            select_query = f"SELECT * FROM sale"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            for row in rows:
                data.append(row)
            return data
        except (Exception, psycopg2.Error) as error:
            print("Error while selecting data from sale:", error)

    @classmethod
    def update_sale_to_db(cls, 
                            sale_id,
                            new_product_id=None, 
                            new_quantity=None, 
                            new_customer_id=None, 
                            new_date=None,
                            new_invoice_address=None,
                            new_delivery_address=None,
                            new_payment_status=None,
                            new_taxes=None,
                            new_total=None,
                            ):
        try:
            update_query = """
            UPDATE sale 
            SET product_id = COALESCE(%s, product_id), 
                quantity = COALESCE(%s, quantity), 
                customer_id = COALESCE(%s, customer_id), 
                date = COALESCE(%s, date),
                invoice_address = COALESCE(%s, invoice_address), 
                delivery_address = COALESCE(%s, delivery_address), 
                payment_status = COALESCE(%s, payment_status),
                taxes = COALESCE(%s, taxes),
                total = COALESCE(%s, total)
            WHERE sale_id = %s
            """
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(update_query, (new_product_id,new_quantity,new_customer_id,new_date,new_invoice_address,new_delivery_address,new_payment_status,new_taxes,new_total,sale_id))
            conn.commit()
            cur.close()
            conn.close()
            print("Sale Updated")
        except (Exception, psycopg2.Error) as error:
            print("Error while updating sale data:", error)

    @classmethod
    def delete_sale_from_db(cls,condition):
        try:
            delete_query = f"DELETE FROM sale WHERE {condition}"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(delete_query)
            conn.commit()
            cur.close()
            conn.close()
            print(f"Sale record is Deleted")
        except (Exception, psycopg2.Error) as error:
            print("Error while deleting sale data:", error)

class Product(ProductCategory, Stock, Customer, Quotation, Sale):
    """
    Python class that is representation of Product in a inventory
    """
    conn = psycopg2.connect(
                host="127.0.0.1",
                database="inventory",
                user="postgres",
                password="password"
            )

    instances = []

    def __init__(
        self,
        product_id : int = None, 
        name : str = None, 
        category_id : int = None,
        price : int = None, 
        description : str = None,
        unit_of_measure : str = None, 
        product_type : str = None,
        in_stock : bool = None,
        color : str = None,
        discount : int = None,
        manufacturer : str = None,
        item_weight : int = None,
        country_of_origin : str = None,
        ):
        self._product_id = product_id
        self._name = name
        self._category_id = category_id
        self._price = price
        self._description = description
        self._unit_of_measure = unit_of_measure
        self._product_type = product_type
        self._in_stock = in_stock
        self._color = color
        self._discount = discount
        self._manufacturer = manufacturer
        self._item_weight = item_weight
        self._country_of_origin = country_of_origin
    
    @property
    def product_id(self):
        return self._product_id
    
    @product_id.setter
    def product_id(self,product_id):
        self._product_id = product_id

    @property
    def name(self):
        return self._name
    
    @name.setter
    def name(self,name):
        self._name = name

    @property
    def category_id(self):
        return self._category_id

    @category_id.setter
    def category_id(self,category_id):
        self._category_id = category_id

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self,price):
        self._price = price

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self,description):
        self._description = description
    
    @property
    def unit_of_measure(self):
        return self._unit_of_measure

    @unit_of_measure.setter
    def unit_if_measure(self, unit_of_measure):
        self._unit_of_measure = unit_of_measure

    @property
    def product_type(self):
        return self._product_type

    @product_type.setter
    def product_type(self, product_type):
        self._product_type = product_type
    
    @property
    def in_stock(self):
        return self._in_stock
    
    @in_stock.setter
    def in_stock(self, in_stock):
        self._in_stock = in_stock

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = color

    @property
    def discount(self):
        return self._discount

    @discount.setter
    def discount(self, discount):
        self._discount = discount

    @property
    def manufacturer(self):
        return self._manufacturer

    @manufacturer.setter
    def manufacturer(self, manufacturer):
        self._manufacturer = manufacturer

    @property
    def item_weight(self):
        return self._item_weight

    @item_weight.setter
    def item_weight(self, item_weight):
        self._item_weight = item_weight

    @property
    def country_of_origin(self):
        return self._country_of_origin

    @country_of_origin.setter
    def country_of_origin(self, country_of_origin):
        self._country_of_origin = country_of_origin

    def get_product_category_name(self, categories, category_id):
        for category in categories:
            if category.category_id == category_id:
                return {
                        "Product Name" : self._name,
                        "Product Category" : category.product_category,
                        "Product Price" : self._price,
                        "Product Color" : self._color,
                        "Product Origin" : self._country_of_origin
                }
        return "Category ID not present"
    
    def get_stock_info(self,stock_details, product_id):
        for stock in stock_details:
            if stock.product_id == product_id:
                return {
                    "Product Name" : self._name,
                    "Quantity":stock.quantity,
                    "Location":stock.location
                }
                
        return "Product not present in Stock"
    
    @classmethod
    def create_product(cls, product_data):
        for data in product_data:
            product = cls(data.get('product_id'),
                        data.get('name'),
                        data.get('category_id'),
                        data.get('price'),
                        data.get('description'),
                        data.get('unit_of_measure'),
                        data.get('product_type'),
                        data.get('in_stock'),
                        data.get('color'),
                        data.get('discount'),
                        data.get('manufacturer'),
                        data.get('item_weight'),
                        data.get('country_of_origin')
                        )
            cls.instances.append(product)
        return cls.instances
    
    @classmethod
    def read_all_products(cls, products):
        for product in products:
            print(f"Product ID: {product.product_id}")
            print(f"Name: {product.name}")
            print(f"Category ID: {product.category_id}")
            print(f"Price: {product.price}")
            print(f"Description: {product.description}")
            print(f"Unit of Measure: {product.unit_of_measure}")
            print(f"Product Type: {product.product_type}")
            print(f"In Stock: {product.in_stock}")
            print(f"Color: {product.color}")
            print(f"Discount: {product.discount}")
            print(f"Manufacturer: {product.manufacturer}")
            print(f"Item Weight: {product.item_weight}")
            print(f"Country of Origin: {product.country_of_origin}")
            print()


    @classmethod
    def update_product(cls, 
               products, 
               product_id, 
               new_name=None, 
               new_category_id=None, 
               new_price=None, 
               new_description=None,
               new_unit_of_measure=None,
               new_product_type=None,
               new_in_stock=None,
               new_color=None,
               new_discount=None,
               new_manufacturer=None,
               new_item_weight=None,
               new_country_of_origin=None
               ):
        for product in products:
            if product.product_id == product_id:
                if new_name is not None:
                    product.name = new_name
                if new_category_id is not None:
                    product.category_id = new_category_id
                if new_price is not None:
                    product.price = new_price
                if new_description is not None:
                    product.description = new_description
                if new_unit_of_measure is not None:
                    product._unit_of_measure = new_unit_of_measure
                if new_product_type is not None:
                    product.product_type = new_product_type
                if new_in_stock is not None:
                    product.in_stock = new_in_stock
                if new_color is not None:
                    product.color = new_color
                if new_discount is not None:
                    product.discount = new_discount
                if new_manufacturer is not None:
                    product.manufacturer = new_manufacturer
                if new_item_weight is not None:
                    product.item_weight = new_item_weight
                if new_country_of_origin is not None:
                    product.country_of_origin =new_country_of_origin
                break

    @classmethod
    def delete(cls, products, product_id):
        for product in products:
            if product.product_id == product_id:
                products.remove(product)
                break
        
    @classmethod
    def insert_product_to_db(cls, sale_data):
        try:
            for data in sale_data:
                product_id = data.product_id
                name = data.name
                category_id = data.category_id
                price = data.price
                description = data.description
                unit_of_measure = data.unit_of_measure
                product_type = data.product_type
                in_stock = data.in_stock
                color = data.color
                discount = data.discount
                manufacturer = data.manufacturer
                item_weight = data.item_weight
                country_of_origin = data.country_of_origin
                conn = cls.conn
                cur = conn.cursor()
                cur.execute("INSERT INTO product (product_id, name,category_id, price,description,unit_of_measure,product_type,in_stock,color,discount,manufacturer,item_weight,country_of_origin) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (product_id, name,category_id, price,description,unit_of_measure,product_type,in_stock,color,discount,manufacturer,item_weight,country_of_origin))
                conn.commit()
            print("Inserted records into product  ")
            cur.close()
            conn.close()
        except (Exception, psycopg2.Error) as error:
            print("Error while inserting product data:", error)

    @classmethod
    def read_product_from_db(cls):
        try:
            data = []
            select_query = f"SELECT * FROM product"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            for row in rows:
                data.append(list(row))
            return data
        except (Exception, psycopg2.Error) as error:
            print("Error while selecting data from product:", error)

    @classmethod
    def update_sale_to_db(cls, 
                            product_id,
                            new_name=None, 
                            new_category_id=None, 
                            new_price=None, 
                            new_description=None,
                            new_unit_of_measure=None,
                            new_product_type=None,
                            new_in_stock=None,
                            new_color=None,
                            new_discount=None,
                            new_manufacturer=None,
                            new_item_weight=None,
                            new_country_of_origin=None,
                            ):
        try:
            update_query = """
            UPDATE product 
            SET name = COALESCE(%s, name), 
                category_id = COALESCE(%s, category_id), 
                price = COALESCE(%s, price), 
                description = COALESCE(%s, description),
                unit_of_measure = COALESCE(%s, unit_of_measure), 
                product_type = COALESCE(%s, product_type),
                in_stock = COALESCE(%s, in_stock), 
                color = COALESCE(%s, color),
                discount = COALESCE(%s, discount),
                manufacturer = COALESCE(%s, manufacturer),
                item_weight = COALESCE(%s, item_weight),
                country_of_origin = COALESCE(%s, country_of_origin)   
            WHERE product_id = %s
            """
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(update_query, (new_name,new_category_id,new_price,new_description,new_unit_of_measure,new_product_type,new_in_stock,new_color,new_discount,new_manufacturer,new_item_weight,new_country_of_origin,product_id))
            conn.commit()
            cur.close()
            conn.close()
            print("Product Updated")
        except (Exception, psycopg2.Error) as error:
            print("Error while updating product data:", error)

    @classmethod
    def delete_product_from_db(cls,condition):
        try:
            delete_query = f"DELETE FROM product WHERE {condition}"
            conn = cls.conn
            cur = conn.cursor()
            cur.execute(delete_query)
            conn.commit()
            cur.close()
            conn.close()
            print(f"Product record is Deleted")
        except (Exception, psycopg2.Error) as error:
            print("Error while deleting product data:", error)